Instrucciones para instalar el servicio
 - Compilar
 - Ejecutar `installutil  PocService.exe` desde el directorio que contiene el ejecutable (normalmente `PocService\bin\Debug\`)
 - Iniciar el servicio

Instrucciones para desinstalar el servicio
  - Detener el servicio
  -  - Ejecutar `installutil /u PocService.exe` desde el directorio que contiene el ejecutable (normalmente `PocService\bin\Debug\`)
